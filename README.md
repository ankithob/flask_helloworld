from flask import Flask
#Here we are importing the Flask module and creating flask webserver from Flask module.

app = Flask(__name__)
# _name_ means current file.My file name app.py.This current file represent my web application.

@app.route("/")
#It represent the default page in browser.

def hello_world():
#When user go to my website then go default page.def defining the function.

    return "Hello World!"

if __name__ == "__main__":
#When run Python script,Python assign the name "_main_" to the script when executed.
    app.run(debug=True,host='0.0.0.0')
#This will run the application.Having debug=True,allow the python errers apper in webpage.

